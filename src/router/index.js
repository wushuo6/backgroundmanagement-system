import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect:"/home/1",
    component: Home,
    children:[
      {
      path: '/home/1',
      name: 'home1',
      component: () => import(/* webpackChunkName: "about" */ '../components/1.vue')
    },
    {
      path: '/home/2',
      name: 'home2',
      component: () => import(/* webpackChunkName: "about" */ '../components/2.vue')
    },
    {
      path: '/home/3',
      name: 'home3',
      component: () => import(/* webpackChunkName: "about" */ '../components/3.vue')
    },
    {
      path: '/home/4',
      name: 'home4',
      component: () => import(/* webpackChunkName: "about" */ '../components/4.vue')
    },
    {
      path: '/home/5',
      name: 'home5',
      component: () => import(/* webpackChunkName: "about" */ '../components/5.vue')
    }, {
      path: '/home/6',
      name: 'home6',
      component: () => import(/* webpackChunkName: "about" */ '../components/6.vue')
    }, {
      path: '/home/7',
      name: 'home7',
      component: () => import(/* webpackChunkName: "about" */ '../components/7.vue')
    }, {
      path: '/home/8',
      name: 'home8',
      component: () => import(/* webpackChunkName: "about" */ '../components/8.vue')
    }, {
      path: '/home/9',
      name: 'home9',
      component: () => import(/* webpackChunkName: "about" */ '../components/9.vue')
    },
   
  ]
    
  },
  {
    path: '/game',
    name: 'Game',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    redirect:"/game/1",
    component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue'),
    children:[
      {
        path: '/game/1',
        name: 'game1',
        component: () => import(/* webpackChunkName: "about" */ '../components/1.vue')
      },
      {
        path: '/game/2',
        name: 'game2',
        component: () => import(/* webpackChunkName: "about" */ '../components/2.vue')
      },
      {
        path: '/game/3',
        name: 'game3',
        component: () => import(/* webpackChunkName: "about" */ '../components/3.vue')
      },
      {
        path: '/game/4',
        name: 'game4',
        component: () => import(/* webpackChunkName: "about" */ '../components/4.vue')
      },
      {
        path: '/game/5',
        name: 'game5',
        component: () => import(/* webpackChunkName: "about" */ '../components/5.vue')
      }, {
        path: '/game/6',
        name: 'game6',
        component: () => import(/* webpackChunkName: "about" */ '../components/6.vue')
      }, {
        path: '/game/7',
        name: 'game7',
        component: () => import(/* webpackChunkName: "about" */ '../components/7.vue')
      }, {
        path: '/game/8',
        name: 'game8',
        component: () => import(/* webpackChunkName: "about" */ '../components/8.vue')
      }, {
        path: '/game/9',
        name: 'game9',
        component: () => import(/* webpackChunkName: "about" */ '../components/9.vue')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
